# Line_Robot -- A robot that can follow a line drawn on the ground

## Description

Based on [Waveshares] JETANK project powered by Nvidia's Jetson Nano.
This robot can follow a line drawn on the ground by utilizing opencv-python
library and [nanocamera]. 

## How to use

For instructions on how to assemble the robot used in this project, please visit [youtube].
Place the robot in front of the line, the frames caught by the camera must include the line
or the robot will automatically stop.

## Visuals -- Robot in action
![](ezgif.com-gif-maker.gif)



## Authors and acknowledgment
@MariaNema
@Douglas_Johansson


[Waveshares]: https://github.com/waveshare/JETANK
[youtube]: https://www.youtube.com/watch?v=qNy1hulFk6I&t=1263s
[nanocamera]: https://github.com/thehapyone/NanoCamera